package ru.example.doges.view

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import ru.example.doges.R

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }

}
