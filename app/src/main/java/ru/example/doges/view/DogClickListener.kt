package ru.example.doges.view

import android.view.View

interface DogClickListener {
    fun onDogClick(v: View)
}