package ru.example.doges.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import ru.example.doges.model.DogBreed
import ru.example.doges.model.DogDatabase

class DetailViewModel(application: Application) : BaseViewModel(application) {

    val dogLiveData = MutableLiveData<DogBreed>()

    fun fetch(uuid: Int) {
        launch {
            val dogDao = DogDatabase(getApplication()).dogDao().getDog(uuid)
            dogLiveData.value = dogDao
        }
    }
}